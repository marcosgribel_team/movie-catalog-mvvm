package com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
data class SearchState(var isEnable: Boolean = false, var query: String = "")
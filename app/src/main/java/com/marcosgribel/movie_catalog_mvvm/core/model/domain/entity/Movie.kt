package com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
@Parcelize
data class Movie(

        @field:Json(name = "id")
        var id: Int,

        @field:Json(name = "overview")
        var overview: String? = null,

        @field:Json(name = "original_language")
        var originalLanguage: String? = null,

        @field:Json(name = "original_title")
        var originalTitle: String? = null,

        @field:Json(name = "video")
        var video: Boolean? = null,

        @field:Json(name = "title")
        var title: String? = null,

        @field:Json(name = "genre_ids")
        var genreIds: List<Int>? = arrayListOf(),

        @field:Json(name = "poster_path")
        var posterPath: String? = null,

        @field:Json(name = "backdrop_path")
        var backdropPath: String? = null,

        @field:Json(name = "release_date")
        var releaseDate: String? = null,

        @field:Json(name = "vote_average")
        var voteAverage: Double? = null,

        @field:Json(name = "popularity")
        var popularity: Double? = null,

        @field:Json(name = "adult")
        var adult: Boolean? = null,

        @field:Json(name = "vote_count")
        var voteCount: Int? = null
) : Parcelable {

    @IgnoredOnParcel
    var imageUrl: String? = null
        get() {
            val path = this.backdropPath
            return path?.let {
                "https://image.tmdb.org/t/p/w500/$it"
            }
        }

}
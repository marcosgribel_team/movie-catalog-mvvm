package com.marcosgribel.movie_catalog_mvvm.core.di

import com.marcosgribel.github_repo_app.core.rx.AppDispatcher
import com.marcosgribel.movie_catalog_mvvm.BuildConfig
import com.marcosgribel.movie_catalog_mvvm.core.model.service.RetrofitConnector
import com.marcosgribel.movie_catalog_mvvm.movie.grid.view.viewmodel.MovieGridViewModel
import com.marcosgribel.movie_catalog_mvvm.movie.model.repository.MovieRepository
import com.marcosgribel.movie_catalog_mvvm.movie.model.repository.MovieRepositoryImpl
import com.marcosgribel.movie_catalog_mvvm.movie.model.service.MovieApi
import com.marcosgribel.movie_catalog_mvvm.movie.model.service.MovieService
import com.marcosgribel.movie_catalog_mvvm.movie.model.service.MovieServiceImpl
import kotlinx.coroutines.Job
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */


private val CoreModule = module {

    single { RetrofitConnector() }

    single { AppDispatcher() }

    single { Job() }
}


private val MovieModule = module {

    single {
        get<RetrofitConnector>().get(get(), MovieApi::class.java)
    }

    single<MovieService> { MovieServiceImpl(BuildConfig.API_KEY, get()) }

    factory<MovieRepository> { MovieRepositoryImpl(get()) }

    viewModel { MovieGridViewModel(get(), get(), get()) }

}

val AppModule = listOf(CoreModule, MovieModule)
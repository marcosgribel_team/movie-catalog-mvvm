package com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
data class Status(val status: String, val message: String? = null) {

    companion object {
        const val LOADING = "LOADING"
        const val SUCCESS = "SUCCESS"
        const val EMPTY = "EMPTY"
        const val ERROR = "ERROR"
    }

}

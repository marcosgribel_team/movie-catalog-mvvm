package com.marcosgribel.movie_catalog_mvvm.movie.model.repository

import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.MovieResponse
import com.marcosgribel.movie_catalog_mvvm.movie.model.service.MovieService
import kotlinx.coroutines.Deferred

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
interface MovieRepository {

  suspend fun nowPlaying(page: Int?): MovieResponse

  suspend fun search(query: String, page: Int?): MovieResponse

}

class MovieRepositoryImpl(private val service: MovieService) : MovieRepository {

  override suspend fun nowPlaying(page: Int?): MovieResponse {
    return service.nowPlaying(page).await()
  }

  override suspend fun search(query: String, page: Int?): MovieResponse = service.search(query, page).await()

}
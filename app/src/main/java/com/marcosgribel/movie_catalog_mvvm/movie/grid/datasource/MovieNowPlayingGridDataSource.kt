package com.marcosgribel.movie_catalog_mvvm.movie.grid.datasource

import androidx.lifecycle.MutableLiveData
import com.marcosgribel.github_repo_app.core.rx.AppDispatcher
import com.marcosgribel.github_repo_app.core.rx.SchedulerProvider
import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.Movie
import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.Status
import com.marcosgribel.movie_catalog_mvvm.movie.model.repository.MovieRepository
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import timber.log.Timber

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class MovieNowPlayingGridDataSource(
        private val status: MutableLiveData<Status>,
        private val repository: MovieRepository,
        private val dispatcher: AppDispatcher,
        private var job: Job
) : MovieGridDataSource() {


    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Movie>) {

        search(nextPage, object : MovieSearchGridDataSourceCallback {
            override fun onSuccess(list: List<Movie>) {
                callback.onResult(list)
            }

            override fun onFailure(throwable: Throwable) {
                attachRetryCallback {
                    loadInitial(params, callback)
                }
            }
        })

    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Movie>) {

        search(nextPage, object : MovieSearchGridDataSourceCallback {
            override fun onSuccess(list: List<Movie>) {
                callback.onResult(list)
            }

            override fun onFailure(throwable: Throwable) {
                attachRetryCallback {
                    loadAfter(params, callback)
                }
            }
        })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Movie>) {
    }

    override fun getKey(item: Movie): Int = item.id


    private fun search(requestedPage: Int, callback: MovieSearchGridDataSourceCallback) {
        status.postValue(Status(Status.LOADING))
        job = CoroutineScope(dispatcher.io()).launch {
            val result = repository.nowPlaying(requestedPage)

            nextPage++
            status.postValue(Status(Status.SUCCESS))
            callback.onSuccess(result.results)
        }
    }


}
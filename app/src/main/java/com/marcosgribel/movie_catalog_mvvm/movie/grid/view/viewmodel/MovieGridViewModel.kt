package com.marcosgribel.movie_catalog_mvvm.movie.grid.view.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.marcosgribel.github_repo_app.core.rx.AppDispatcher
import com.marcosgribel.github_repo_app.core.rx.SchedulerProvider
import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.Movie
import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.SearchState
import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.Status
import com.marcosgribel.movie_catalog_mvvm.movie.grid.datasource.factory.MovieGridDataSourceFactory
import com.marcosgribel.movie_catalog_mvvm.movie.model.repository.MovieRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class MovieGridViewModel(
        private val repository: MovieRepository,
        private val dispatcher: AppDispatcher,
        private var job: Job
) : ViewModel() {

    var searchState: MutableLiveData<SearchState> = MutableLiveData()
    val status = MutableLiveData<Status>()
    var listOfMovie: LiveData<PagedList<Movie>> = MutableLiveData<PagedList<Movie>>()


    private val dataSourceFactory = MovieGridDataSourceFactory(status, repository, dispatcher, job)


    init {

        val pageSize = 20
        val pagedListConfig = PagedList.Config.Builder()
                .setInitialLoadSizeHint(pageSize)
                .setPageSize(pageSize)
                .setPrefetchDistance(pageSize / 2)
                .setEnablePlaceholders(false)
                .build()

        listOfMovie = LivePagedListBuilder<Int, Movie>(dataSourceFactory, pagedListConfig)
                .build()

        dataSourceFactory.create()

    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }

    fun onSearch(query: String?) {
        query?.let {
            CoroutineScope(dispatcher.main()).launch {
                delay(500)
                onUpdateSearchState(true, it.trim())
                onReload()
            }
        }
    }


    fun onSearchClose() {
        onUpdateSearchState(false)
        onReload()
    }

    private fun onUpdateSearchState(enable: Boolean, query: String = ""){
        val state = SearchState(enable, query)
        dataSourceFactory.searchState = state
        searchState.postValue(state)
    }

   private fun onReload(){
        dataSourceFactory.dataSource.value?.invalidate()
    }

}


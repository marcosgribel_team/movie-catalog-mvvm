package com.marcosgribel.movie_catalog_mvvm.movie.grid.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marcosgribel.article_collection_app.core.extensions.loadUrl
import com.marcosgribel.movie_catalog_mvvm.R
import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.Movie
import kotlinx.android.synthetic.main.item_movie_grid.view.*

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class MovieGridViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    companion object {

        fun inflate(parent: ViewGroup): MovieGridViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_movie_grid, parent, false)
            return MovieGridViewHolder(view)
        }

    }


    fun bindData(data: Movie, callback : MovieGridAdapter.MovieGridAdapterCallback) {

        view.image_view_movie_grid.loadUrl(data.imageUrl, R.drawable.ic_launcher_background)
        view.setOnClickListener {
            callback.onItemSelected(data)
        }
    }

}
package com.marcosgribel.movie_catalog_mvvm.movie.grid.view.ui

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.paging.PagedList
import androidx.recyclerview.widget.GridLayoutManager
import com.marcosgribel.movie_catalog_mvvm.R
import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.Movie
import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.Status
import com.marcosgribel.movie_catalog_mvvm.movie.grid.view.adapter.MovieGridAdapter
import com.marcosgribel.movie_catalog_mvvm.movie.grid.view.viewmodel.MovieGridViewModel
import kotlinx.android.synthetic.main.fragment_movie_grid.*
import org.koin.android.ext.android.inject

/**
 * A simple [Fragment] subclass.
 *
 */
class MovieGridFragment : Fragment() {

  private val viewModel: MovieGridViewModel by inject()
  private val adapter = MovieGridAdapter(onAdapterCallBack())

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setHasOptionsMenu(true)
    retainInstance = true
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                            savedInstanceState: Bundle?): View? {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_movie_grid, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    initialize()

  }

  /**

  MENU

   */

  override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
    super.onCreateOptionsMenu(menu, inflater)
    activity?.menuInflater?.inflate(R.menu.menu_movie_grid_fragment, menu)
    initialize(menu)
  }

  /*

   */
  private fun onSearchActionExpandListener(): MenuItem.OnActionExpandListener {
    return object : MenuItem.OnActionExpandListener {
      override fun onMenuItemActionExpand(p0: MenuItem?): Boolean {
        return true
      }

      override fun onMenuItemActionCollapse(p0: MenuItem?): Boolean {
        viewModel.onSearchClose()
        return true
      }
    }
  }

  private fun onSearchCloseListener(): SearchView.OnCloseListener {
    return SearchView.OnCloseListener {
      viewModel.onSearchClose()
      true
    }
  }

  /*

   */
  private fun onSearchQueryTextListener(): SearchView.OnQueryTextListener {
    return object : SearchView.OnQueryTextListener {
      override fun onQueryTextSubmit(query: String?): Boolean {
        return true
      }

      override fun onQueryTextChange(newText: String?): Boolean {
        viewModel.onSearch(newText)
        return true
      }
    }
  }

  /*

   */
  private fun initialize() {
    val linearLayoutManager = GridLayoutManager(context, 2)
    recycler_grid_movie.layoutManager = linearLayoutManager
    recycler_grid_movie.setHasFixedSize(true)
    recycler_grid_movie.adapter = adapter

    viewModel.listOfMovie.observe(this, onListOfMovieObserve())
  }

  /*

   */
  private fun initialize(menu: Menu?) {

    val searchItem = menu?.let {
      it.findItem(R.id.menu_search_action)
    }
    searchItem?.setOnActionExpandListener(onSearchActionExpandListener())

    val searchView = searchItem?.actionView as SearchView

    viewModel.searchState.value?.let {
      if (it.isEnable) {
        searchItem.expandActionView()
        searchView.setQuery(it.query, false)
      } else {
        searchItem.collapseActionView()
      }
    }

    searchView.setOnQueryTextListener(onSearchQueryTextListener())
    searchView.setOnCloseListener(onSearchCloseListener())

    viewModel.status.observe(this, Observer<Status> {
      when (it.status) {
        Status.LOADING -> {
          onLoading()
        }
        Status.SUCCESS -> {
          onSuccess()
        }
      }
    })

  }

  private fun onListOfMovieObserve(): Observer<PagedList<Movie>> {
    return Observer { t -> adapter.submitList(t) }
  }

  private fun onAdapterCallBack(): MovieGridAdapter.MovieGridAdapterCallback {
    return object : MovieGridAdapter.MovieGridAdapterCallback {
      override fun onItemSelected(movie: Movie) {
        val directions = MovieGridFragmentDirections.ActionMovieGridFragmentToMovieDetailFragment(movie)
        val navController = Navigation.findNavController(view!!)
        navController.saveState()
        navController.navigate(directions)
      }
    }
  }

  private fun onLoading() {
    view?.let {
      it.findViewById<View>(R.id.progress_bar).visibility = View.VISIBLE
      it.findViewById<View>(R.id.txt_empty).visibility = View.GONE
    }
  }

  private fun onSuccess() {
    view?.let {
      it.findViewById<View>(R.id.progress_bar).visibility = View.GONE
      it.findViewById<View>(R.id.txt_empty).visibility = View.GONE
      it.findViewById<View>(R.id.recycler_grid_movie).visibility = View.VISIBLE
    }
  }

}

package com.marcosgribel.movie_catalog_mvvm.movie.grid.datasource

import androidx.paging.ItemKeyedDataSource
import com.marcosgribel.movie_catalog_mvvm.core.callback.RetryCallback
import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.Movie

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
abstract class MovieGridDataSource : ItemKeyedDataSource<Int, Movie>() {

    protected interface MovieSearchGridDataSourceCallback {

        fun onSuccess(list: List<Movie>)

        fun onFailure(throwable: Throwable)

    }


    var nextPage = 1
    private var retryCallback: RetryCallback? = null

    protected fun attachRetryCallback(callback: () -> Unit) {
        retryCallback = object : RetryCallback {
            override fun retry() {
                callback()
            }
        }
    }
}
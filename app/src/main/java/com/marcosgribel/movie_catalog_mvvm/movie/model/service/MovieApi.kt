package com.marcosgribel.movie_catalog_mvvm.movie.model.service

import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.MovieResponse
import io.reactivex.Single
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
interface MovieApi {

    @GET("/3/movie/now_playing")
    fun nowPlaying(
            @Query("api_key") apiKey: String,
            @Query("page") page: Int?
    ) : Deferred<MovieResponse>

    @GET("/3/search/movie")
    fun search(@Query("api_key") apiKey: String,
               @Query("query") query: String,
               @Query("page") page: Int?
    ) : Deferred<MovieResponse>

}
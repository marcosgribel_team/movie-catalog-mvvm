package com.marcosgribel.movie_catalog_mvvm.movie.model.service

import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.MovieResponse
import io.reactivex.Single
import kotlinx.coroutines.Deferred

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
interface MovieService {

    fun nowPlaying(page: Int?) : Deferred<MovieResponse>

    fun search(query: String, page: Int?) : Deferred<MovieResponse>

}


class MovieServiceImpl(private val apiKey: String, private val api: MovieApi) : MovieService {

    override fun nowPlaying(page: Int?): Deferred<MovieResponse> = api.nowPlaying(apiKey, page)

    override fun search(query: String, page: Int?): Deferred<MovieResponse> = api.search(apiKey, query, page)

}
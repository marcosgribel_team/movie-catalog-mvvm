package com.marcosgribel.movie_catalog_mvvm.movie.grid.view.adapter

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.Movie

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class MovieGridAdapter(private val callback: MovieGridAdapterCallback) : PagedListAdapter<Movie, MovieGridViewHolder>(COMPARATOR_ITEM_CALLBACK) {


    interface MovieGridAdapterCallback {

        fun onItemSelected(movie: Movie)

    }


    companion object {

        private val COMPARATOR_ITEM_CALLBACK = object : DiffUtil.ItemCallback<Movie>() {

            override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean = oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean = oldItem.title == newItem.title
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieGridViewHolder {
        return MovieGridViewHolder.inflate(parent)
    }

    override fun onBindViewHolder(holder: MovieGridViewHolder, position: Int) {
        val movie = getItem(position)
        holder.bindData(movie!!, callback)

    }
}
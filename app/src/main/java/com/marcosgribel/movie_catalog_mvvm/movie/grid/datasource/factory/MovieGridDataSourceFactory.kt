package com.marcosgribel.movie_catalog_mvvm.movie.grid.datasource.factory

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.marcosgribel.github_repo_app.core.rx.AppDispatcher
import com.marcosgribel.github_repo_app.core.rx.SchedulerProvider
import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.Movie
import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.SearchState
import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.Status
import com.marcosgribel.movie_catalog_mvvm.movie.grid.datasource.MovieGridDataSource
import com.marcosgribel.movie_catalog_mvvm.movie.grid.datasource.MovieNowPlayingGridDataSource
import com.marcosgribel.movie_catalog_mvvm.movie.grid.datasource.MovieSearchGridDataSource
import com.marcosgribel.movie_catalog_mvvm.movie.model.repository.MovieRepository
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Job
import timber.log.Timber

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class MovieGridDataSourceFactory(
        private val status: MutableLiveData<Status>,
        private val repository: MovieRepository,
        private val dispatcher: AppDispatcher,
        private var job: Job
) : DataSource.Factory<Int, Movie>() {


    init {

        Timber.tag(MovieGridDataSourceFactory::class.java.simpleName.toString())
    }

    var dataSource = MutableLiveData<MovieGridDataSource>()
    private var _dataSource: MovieGridDataSource? = null

    var searchState = SearchState(false, "")

    override fun create(): DataSource<Int, Movie> {
        _dataSource = if (searchState == null || !searchState.isEnable) {
            MovieNowPlayingGridDataSource(status, repository, dispatcher, job)
        } else {
            MovieSearchGridDataSource(searchState.query, status, repository, dispatcher, job)
        }

        dataSource.postValue(_dataSource)

        return _dataSource as MovieGridDataSource
    }

}
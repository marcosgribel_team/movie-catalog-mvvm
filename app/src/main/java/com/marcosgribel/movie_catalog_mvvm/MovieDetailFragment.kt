package com.marcosgribel.movie_catalog_mvvm


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.marcosgribel.article_collection_app.core.extensions.loadUrl
import com.marcosgribel.movie_catalog_mvvm.core.model.domain.entity.Movie
import kotlinx.android.synthetic.main.fragment_movie_detail.*

/**
 * A simple [Fragment] subclass.
 *
 */
class MovieDetailFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val args = MovieDetailFragmentArgs.fromBundle(arguments)

        initialize(args.movie)

    }


    private fun initialize(movie: Movie) {
        image_head_movie_detail.loadUrl(movie.imageUrl, R.drawable.ic_launcher_background)
        text_title_movie_detail.text = movie.title
        text_description_movie_detail.text = movie.overview
    }

}

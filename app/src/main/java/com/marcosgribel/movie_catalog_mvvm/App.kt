package com.marcosgribel.movie_catalog_mvvm

import android.app.Application
import com.marcosgribel.movie_catalog_mvvm.core.di.AppModule
import org.koin.android.ext.android.startKoin
import timber.log.Timber

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, AppModule)

        if(BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }

}
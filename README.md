# Movie Catalog 


### Get Started ###

* [Android Studio Preview](https://developer.android.com/studio/preview/)
* [Android JetPack](https://developer.android.com/jetpack/)
* [Java 1.8](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)



## Project


This project leverages on several modern approaches to build Android applications :

- 100% written in Kotlin programming language.
- [MVVM Architecture](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel). powered by Android JetPack
- Easy code modularization 
    - All packages is organized following the pattern "package by feature", that way is easily to modularize the project when it expand and also in case the teams who works based in features is a good strategy to modularize it by team.
- Based on [Material Design](https://material.io/design/) guideline.
- Single Activity application
- Clean Code practices


## Libraries
- Koin
- RxJava/RxAndroid
- Architecture JetPack
    - Paging Library
	- Live Data
	- ViewModel
    - Navigation
- Retroit
- Glide


## Screenshots 

You may check some [screenshoots](https://bitbucket.org/marcosgribel_team/movie-catalog-mvvm/src/master/screenshots/) from the result application in screenshot gallery. 


